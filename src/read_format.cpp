#include <iostream>

#include "lua.hpp"
#include "bindings.hpp"

config _conf;

int main (int argc, char** argv) {
    if (argc < 3) {
        fprintf(stderr, "missing arguments\n");
        return EXIT_FAILURE;
    }

    _conf.file.open(argv[2], std::ios::binary);

    if (!_conf.file) {
        perror(argv[2]);
        return EXIT_FAILURE;
    }

    _conf.filename = argv[2];
    _conf.show = true;
    _conf.warn = true;

    lua_State* L = luaL_newstate();
    luaL_openlibs(L);
    register_bindings(L);
    int err = luaL_dofile(L, argv[1]);
    if (err) {
        std::cerr << lua_tostring(L, -1) << std::endl;
        return EXIT_FAILURE;
    }
    return EXIT_SUCCESS;
}