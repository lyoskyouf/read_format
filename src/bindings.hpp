#ifndef BINDINGS_H
#define BINDINGS_H

#include "lua.hpp"
#include <fstream>

struct config {    
    std::ifstream file;
    std::string filename;
    bool show;
    bool warn;
};

extern config _conf;

void register_bindings(lua_State* L);

#endif