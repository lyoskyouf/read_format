#include "bindings.hpp"
#include <functional>
#include <iostream>
#include <string>
#include <vector>
#include <limits>

template<typename Num>
void showNum(Num n) {
    std::cout << +n;
}


template<typename Num>
void showHex(Num n) {
    auto flags = std::cout.flags();
    std::cout << std::hex << std::showbase;
    std::cout << +n;
    std::cout.flags(flags);
}

void showChar(char c) {
    if (isprint(c)) {
        std::cout << c;
    }
    else {
        switch(c) {
            case '\0': std::cout << "\\0"; break;
            case '\t': std::cout << "\\t"; break;
            case '\n': std::cout << "\\n"; break;
            default: showHex(c); break;
        }
    }
}

void showString(char* str, size_t len) {
    for (size_t i = 0; i < len; i++) {
        showChar(str[i]);
    }
    std::cout << std::endl;
}

template<typename Num,
         typename LuaNum,
         void (*pushNum) (lua_State*, LuaNum),
         void (*showNum) (Num)
        >
int read_unit(lua_State* L) {
    int npar = lua_gettop(L);
    Num val = 0;
    std::vector<Num> values;
    if (npar == 0) {
        _conf.file.read((char*)&val, sizeof(Num));
        if (_conf.show)
            std::cout << val << std::endl;
        pushNum(L, val);
        return 1;
    }
    else if (npar == 1) {
        LuaNum nval = lua_tonumber(L, -1);
        if (nval <= 0) {
            std::cerr << "error: zero or negative read size." << std::endl;
            lua_pushnil(L);
            return 1;
        }
        lua_createtable(L, nval, 0);
        for (int i = 0; i < nval; i++) {
            _conf.file.read((char*)&val, sizeof(Num));
            if (_conf.show) values.push_back(val);
            lua_pushinteger(L, i+1);
            pushNum(L, val);
            lua_settable(L, -3);
        }
        if (_conf.show) {
            std::cout << "[";
            for (int i = 0; i < nval; i++) {
                if (i > 0) std::cout << ", ";
                showNum(values.at(i));
            }
            std::cout << "]" << std::endl;
        }
        return 1;
    }
    else {
        std::cerr << "error: too many parameters." << std::endl;
        lua_pushnil(L);
        return 1;
    }
}

template<typename Num,
         typename LuaNum,
         void (*pushNum) (lua_State*, LuaNum),
         void (*showNum) (Num)
        >
int read_matrix(lua_State* L) {
    int npar = lua_gettop(L);
    std::vector<Num> values;
    Num val = 0;
    if (npar < 2) {
        std::cerr << "error: missing parameters" << std::endl;
        lua_pushnil(L);
        return 1;
    }
    else if (npar == 2) {
        LuaNum nlines = lua_tonumber(L, 1);
        LuaNum ncols = lua_tonumber(L, 2);
        if (nlines <= 0 || ncols <= 0) {
            std::cerr << "error: zero or negative lines or cols." << std::endl;
            lua_pushnil(L);
            return 1;
        }
        lua_createtable(L, nlines, 0);
        for (int i = 0; i < nlines; i++) {
            lua_pushinteger(L, i+1);
            lua_createtable(L, ncols, 0);
            for (int j = 0; j < ncols; j++) {
                _conf.file.read((char*)&val, sizeof(Num));
                if (_conf.show) values.push_back(val);
                lua_pushinteger(L, j+1);
                pushNum(L, val);
                lua_settable(L, -3);
            }
            lua_settable(L, -3);
        }
        if (_conf.show) {
            std::cout << "[" << std::endl;
            for (int i = 0; i < nlines; i++) {
                if (i > 0) std::cout << ", " << std::endl;
                std::cout << "  [";
                for (int j = 0; j < ncols; j++) {
                    if (j > 0) std::cout << ", ";
                    showNum(values.at(i * ncols + j));
                }
                std::cout << "]";
            }
            std::cout << std::endl << "]" << std::endl;
        }
        return 1;
    }
    else {
        std::cerr << "error: too many parameters." << std::endl;
        lua_pushnil(L);
        return 1;
    }
}

int read_string(lua_State* L) {
    int npar = lua_gettop(L);
    if (npar == 0) {
        // null terminated string
        std::vector<char> buf;
        char c;
        while (_conf.file.get(c) && c != '\0')
            buf.push_back(c);
        if (_conf.show)
            showString(buf.data(), buf.size());
        lua_pushstring(L, buf.data());
        return 1;
    }
    else if (npar == 1) {
        // fixed size string (can contain null)
        lua_Number size = lua_tonumber(L, 1);
        if (size <= 0) {
            std::cerr << "error: zero or negative read size" << std::endl;
            lua_pushnil(L);
            return 1;
        }
        std::vector<char> buf (size);
        _conf.file.read(buf.data(), size);
        lua_pushlstring(L, buf.data(), size);
        showString(buf.data(), size);
        return 1;
    }
    else {
        lua_pushnil(L);
        return 1;
    }
}

int skip_bytes(lua_State* L) {
    int npar = lua_gettop(L);
    if (npar == 0) {
        std::cerr << "error: no parameters" << std::endl;
        return 0;
    }
    else if (npar > 1) {
        std::cerr << "error: too many parameters" << std::endl;
        return 0;
    }
    else {
        lua_Integer nskip = lua_tointeger(L, 1);
        if (nskip < 0) {
            std::cerr << "error: negative skip" << std::endl;
            return 0;
        }
        if (nskip == 0 && _conf.warn)
            std::cout << "warning: skipping zero bytes" << std::endl;
        _conf.file.ignore(nskip);
        return 0;
    }
}

template <typename Num, void (*showNum)(Num) = showNum>
int read_integer(lua_State* L) {
    return read_unit<Num, lua_Integer, lua_pushinteger, showNum>(L);
}

template <typename Num, void (*showNum)(Num) = showNum>
int read_number(lua_State* L) {
    return read_unit<Num, lua_Number, lua_pushnumber, showNum>(L);
}

template <typename Num, void (*showNum)(Num) = showNum>
int read_matrix_integer(lua_State* L) {
    return read_matrix<Num, lua_Integer, lua_pushinteger, showNum>(L);
}

template <typename Num, void (*showNum)(Num) = showNum>
int read_matrix_number(lua_State* L) {
    return read_matrix<Num, lua_Number, lua_pushnumber, showNum>(L);
}

void register_bindings(lua_State* L) {
    const luaL_Reg reg[] = {
        {"int8", read_integer<int8_t>},
        {"int16", read_integer<int16_t>},
        {"int32", read_integer<int32_t>},
        {"int64", read_integer<int64_t>},

        {"uint8", read_integer<uint8_t>},
        {"uint16", read_integer<uint16_t>},
        {"uint32", read_integer<uint32_t>},
        {"uint64", read_integer<uint64_t>},

        {"float", read_number<float>},
        {"double", read_number<double>},

        {"int", read_integer<int>},
        {"size_t", read_integer<size_t>},
        {"char", (read_integer<char, showChar>)},

        {"byte", (read_integer<uint8_t, showHex>)},

        {"mat_int8", read_matrix_integer<int8_t>},
        {"mat_int16", read_matrix_integer<int16_t>},
        {"mat_int32", read_matrix_integer<int32_t>},
        {"mat_int64", read_matrix_integer<int64_t>},

        {"mat_uint8", read_matrix_integer<uint8_t>},
        {"mat_uint16", read_matrix_integer<uint16_t>},
        {"mat_uint32", read_matrix_integer<uint32_t>},
        {"mat_uint64", read_matrix_integer<uint64_t>},

        {"mat_float", read_matrix_number<float>},
        {"mat_double", read_matrix_number<double>},

        {"mat_int", read_matrix_integer<int>},
        {"mat_byte", (read_matrix_integer<uint8_t, showHex>)},

        {"str", read_string},

        {"skip", skip_bytes},
        {NULL, NULL}
    };
    lua_pushglobaltable(L);
    luaL_setfuncs(L, reg, 0);
    lua_pop(L, 1);
}