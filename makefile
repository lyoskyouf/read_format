LUA_PLAT= posix

all: lua src

.PHONY: lua src clean

lua:
	@echo "make lua..."
	@cd lua && $(MAKE) -s $(LUA_PLAT)

src: lua
	@echo "make src..."
	@$(MAKE) -C src all
	cp src/read_format ./read_format

clean:
	@echo "clean lua..."
	@cd lua && $(MAKE) -s clean
	@echo "clean src..."
	@cd src && $(MAKE) -s clean
	rm -f read_format
